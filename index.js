var CROSS = 'X';
var ZERO = 'O';
var EMPTY = ' ';


let flag = true;
let board = [];
let step = 0;


startGame();

function startGame () {
  let size = prompt("Введите размер игрового поля: ", 3);
  renderGrid(size);
  fillGrid(board, size);
}

function fillGrid(field, size) {
  for (let i = 0; i < size; i++) {
    field[i] = [];
    for (let j = 0; j < size; j++) {
      field[i][j] = EMPTY;
    }
  }
}

/* обработчик нажатия на клетку */
function cellClickHandler (row, col) {
  console.log(`Clicked on cell: ${row}, ${col}`); 

  if (board[row][col] !== EMPTY) {
    alert("Ход невозможен");
    return;
  } 
  
  if(!(step === (board.length * board.length)) && flag) {
    renderSymbolInCell(CROSS, row, col);
    board[row][col] = CROSS;
    step++;

    if (hasWinSequence(board, CROSS)) {
      showMessage("Крестики выиграли");
      flag = false;
    }
  }

  if(!(step === (board.length * board.length)) && flag) {
    moveAI(board);
    step++;

    if (hasWinSequence(board, ZERO)) {
      showMessage("Нолики выиграли");
      flag = false;
    }
  }

  if ((step === (board.length * board.length)) && flag) {
    showMessage("Победила дружба");
    flag = false;
  }


}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler () {
  console.log('reset!');
  flag = true;
  step = 0;
  showMessage(" ");

  startGame();

}

function hasWinSequence(board, sign) {
  for (let i = 0; i < board.length; i++) {
    if (isLine(board, sign, 0, i, 1, 0) || 
        isLine(board, sign, i, 0, 0, 1) || 
        isLine(board, sign, 0, 0, 1, 1) || 
        isLine(board, sign, board.length - 1, 0, -1, 1)) return true;
  }

  return false;
}

function isLine(board, sign, x0, y0, dx, dy) {
	for(let i = 0; i < board.length; i++) {
    if (board[x0 + i * dx][y0 + i * dy] !== sign) { 
      return false;
    }
  }

  colorizeLine(board, sign, x0, y0, dx, dy);
  return true;
}

function colorizeLine(board, sign, x0, y0, dx, dy) {
  for(let i = 0; i < board.length; i++) {
    renderSymbolInCell(sign, x0 + i * dx, y0 + i * dy, "red");
  }
}

function moveAI(board) {
  let x = [];
  let y = [];

  for(let i = 0; i < board.length; i++) {
    for(let j = 0; j < board.length; j++) {
      if(board[i][j] === EMPTY) {
        x.push(i);
        y.push(j);
      }
    }
  }

  let choosenCell = Math.floor(Math.random() * x.length);
  console.log(x[choosenCell],y[choosenCell]);
  board[ x[choosenCell] ][ y[choosenCell] ] = ZERO;
  renderSymbolInCell(ZERO, x[choosenCell], y[choosenCell]);
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid (dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell (symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell (row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin () {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw () {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell (row, col) {
  findCell(row, col).click();
}
